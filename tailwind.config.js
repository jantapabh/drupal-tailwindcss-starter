const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  // Drupal views will parse out the default tailwindcss colon (:) so we use a double underscore instead (__)
  separator: '__',
  // Files to scan for classes
  content: [
    './templates/**/*.twig',
    './components/**/*.html',
    './drupal_tailwindcss_starter.theme',
    '../../../../config/**/*.yml'
  ],
  // Turn off purging in development environment
  safelist: process.env.NODE_ENV === "development" ? [{ pattern: /.*/ }] : [],
  plugins: [
    // TailwindCSS typography classes
    require('@tailwindcss/typography'),
    // TailwindCSS form classes
    require('@tailwindcss/forms'),
    // Debugging visual aids
    require('tailwind-debug-mode')({
      wireColor: '#000000cc', // the color of the wires
      svgColor: '#000000cc', // the color of the svg
      textColor: '#33333399', // the color of the text on hover
      inputColor: '#33333322', // the background color of input elements
    }),
  ]
};
